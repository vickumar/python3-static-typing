import logging
import sys
import unittest

from static_tests.FunctionTests import FunctionTests
from static_tests.CallablesTest import CallablesTest
from static_tests.GenericsTests import GenericsTests

logger = logging.getLogger()
logger.level = logging.DEBUG
stream_handler = logging.StreamHandler(sys.stdout)
logger.addHandler(stream_handler)


class TestStaticTypes(unittest.TestCase, FunctionTests, CallablesTest, GenericsTests):

    def setUp(self):
        self.logger = logger

    def assertType(self, a, b, err_msg=""):
        if err_msg:
            self.assertTrue(type(a) is b, err_msg)
        else:
            self.assertTrue(type(a) is b)

    # CHECK SOME DEFAULT TYPES
    def test_0_check_types(self):
        test_str = "abc"
        test_int = 7
        test_float = 1.234
        test_list = [1, 2, 3]
        test_tuple = (1, 2, 3)
        test_dict = {"key1": "key2", "key3": "key4"}

        self.assertType(test_str, str, "type <String> != <String>")
        self.logger.info("A string is a string")

        self.assertType(test_int, int, "type <Int> != <Int>")
        self.logger.info("An int is an int")

        self.assertType(test_float, float, "type <Float> != <Float>")
        self.logger.info("An float is a float")

        self.assertType(test_list, list, "type <List> != <List>")
        self.logger.info("A list is a list")

        self.assertType(test_tuple, tuple, "type <Tuple> != <Tuple>")
        self.logger.info("A tuple is a tuple")

        self.assertType(test_dict, dict, "type <Dict> != <Dict>")
        self.logger.info("A dictionary is a dictionary")


if __name__ == "__main__":
    loader = unittest.TestLoader()
    suite = unittest.TestSuite((
        loader.loadTestsFromTestCase(TestStaticTypes),
    ))
    runner = unittest.TextTestRunner(verbosity=2)
    runner.run(suite)
