from typing import Callable
from static_tests.FunctionTests import FunctionTests


class CallablesTest():
    # CHECK USING CALLABLES
    @staticmethod
    def _greeting_proxy(greeting: Callable[[str], str], name: str) -> str:
        return greeting(name)

    def test_4_make_function_call_with_callable(self):
        _greeting = self._greeting_proxy(FunctionTests.greeting, "proxy")
        self.assertType(_greeting, str)
        self.logger.info("Greeting via proxy: %s" % _greeting)

    def test_5_callable_with_bad_args(self):
        _greeting = self._greeting_proxy(FunctionTests.greeting, 123)
        self.assertType(_greeting, str)
        self.logger.info("Greeting via proxy: %s" % _greeting)
        self.logger.info("Callables, like functions, work with bad arguments")