from typing import Generic, TypeVar

A = TypeVar('A')
B = TypeVar('B', covariant=True)


class Animal:
    def __init__(self):
        pass


class Dog(Animal):
    def __init__(self):
        pass


class PetList(Generic[A]):
    def __init__(self):
        pass


class PetList2(Generic[B]):
    def __init__(self):
        pass


class GenericsTests():
    # GENERICS, TEST INVARIANCE and COVARIANCE
    def test_6_generics_are_invariant_by_default(self):
        _animals = PetList[Animal]
        _dogs = PetList[Dog]
        self.assertFalse(issubclass(_dogs, _animals))
        self.logger.info("Generics are invariant by default")

    def test_7_generics_can_be_covariant(self):
        _animals = PetList2[Animal]
        _dogs = PetList2[Dog]
        self.assertTrue(issubclass(_dogs, _animals))
        self.logger.info("Generics can be made to be covariant")