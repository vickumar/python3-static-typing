
class FunctionTests():

    @staticmethod
    def greeting(name: str) -> str:
        return "Hello %s" % name

    @staticmethod
    def greeting_bad_return_value(name: str) -> str:
        return 55

     # CHECK CALLING FUNCTIONS
    def test_1_make_a_function_call(self):
        _greeting = FunctionTests.greeting("there")
        self.logger.info("Greeting: %s" % _greeting)
        self.assertType(_greeting, str)

    def test_2_make_function_call_with_bad_args(self):
        _bad_greetings = [55, 1.23, [1, 2, 3], {"key1": "key2"}]
        for _bad_greeting in _bad_greetings:
            _greeting_test = FunctionTests.greeting(_bad_greeting)
            self.assertType(_greeting_test, str)
            self.logger.info("Greeting with %s: %s" % (str(type(_bad_greeting)), _greeting_test))

    def test_3_make_function_call_with_bad_return_value(self):
        _bad_greeting_return_value = FunctionTests.greeting_bad_return_value("something")
        self.assertType(_bad_greeting_return_value, int)
        self.logger.info("Got bad return value: %s" % _bad_greeting_return_value)
